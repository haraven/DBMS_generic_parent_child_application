﻿using System;
using System.Windows.Forms;

namespace lab_4
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            string connection_string = ConfigParser.GetConfigValue("connection-string"),
                        parent_table = ConfigParser.GetConfigValue("parent-table"),
                         child_table = ConfigParser.GetConfigValue("child-table"),
                         child_fk    = ConfigParser.GetConfigValue("child-fk-name");

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ParentForm(connection_string, parent_table, child_table, child_fk));
        }
    }
}
