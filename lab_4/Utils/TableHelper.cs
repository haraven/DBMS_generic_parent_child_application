﻿using System;
using System.Data.SqlClient;

namespace lab_4.Utils
{
    public class TableHelper
    {
        public static string GetTablePK(string connection_string, string table_name)
        {
            if (table_name.StartsWith("dbo."))
                table_name = table_name.Substring(4);
            using (SqlConnection conn = new SqlConnection(connection_string))
            {
                try
                {
                    string select_query = @"SELECT COLUMN_NAME
                    FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
                    WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + CONSTRAINT_NAME), 'IsPrimaryKey') = 1 AND TABLE_NAME = '" + table_name + "'";
                    conn.Open();
                    SqlCommand my_cmd = new SqlCommand(select_query, conn);
                    SqlDataReader reader = my_cmd.ExecuteReader();
                    string res = "";
                    if (reader.HasRows)
                    {
                        reader.Read();
                        res = reader.GetString(0);
                    }
                    reader.Close();

                    return res;
                }
                catch (Exception)
                {
                    return "";
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        public static string GetTablePK(SqlConnection connection, string table_name)
        {
            if (table_name.StartsWith("dbo."))
                table_name = table_name.Substring(4);
            string select_query = @"SELECT COLUMN_NAME
                    FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
                    WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + CONSTRAINT_NAME), 'IsPrimaryKey') = 1 AND TABLE_NAME = '" + table_name + "'";
            if (connection.State != System.Data.ConnectionState.Open)
                connection.Open();
            SqlCommand my_cmd = new SqlCommand(select_query, connection);
            SqlDataReader reader = my_cmd.ExecuteReader();
            string res = "";
            if (reader.HasRows)
            {
                reader.Read();
                res = reader.GetString(0);
            }

            reader.Close();
            connection.Close();
            return res;
        }

        public static System.Data.SqlDbType GetColType(SqlConnection connection, string table_name, string colname)
        {
            string sql = @"SELECT DATA_TYPE 
                FROM INFORMATION_SCHEMA.COLUMNS
                WHERE TABLE_NAME = '" + table_name + "' AND COLUMN_NAME = '" + colname + "'";
            if (connection.State != System.Data.ConnectionState.Open)
                connection.Open();
            SqlCommand my_cmd = new SqlCommand(sql, connection);
            SqlDataReader reader = my_cmd.ExecuteReader();
            System.Data.SqlDbType res = System.Data.SqlDbType.Bit;
            if (reader.HasRows)
            {
                reader.Read();
                string coltype = reader.GetString(0);
                switch (coltype)
                {
                    case "int":
                        res = System.Data.SqlDbType.Int;
                        break;
                    case "varchar":
                        res = System.Data.SqlDbType.VarChar;
                        break;
                    case "nvarchar":
                        res = System.Data.SqlDbType.NVarChar;
                        break;
                    case "decimal":
                        res = System.Data.SqlDbType.Decimal;
                        break;
                    default:
                        break;
                }
            }

            reader.Close();

            return res;
        }
    }
}
