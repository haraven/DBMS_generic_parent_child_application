﻿using System.Configuration;

namespace lab_4
{
    public class ConfigParser
    {
        public static string GetConfigValue(string node_name)
        {
            return ConfigurationManager.AppSettings[node_name];
        }
    }
}
