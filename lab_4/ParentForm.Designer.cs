﻿namespace lab_4
{
    partial class ParentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.parent_panel = new System.Windows.Forms.TableLayoutPanel();
            this.show_child_btn = new System.Windows.Forms.Button();
            this.parent_grid = new System.Windows.Forms.DataGridView();
            this.parent_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.parent_grid)).BeginInit();
            this.SuspendLayout();
            // 
            // parent_panel
            // 
            this.parent_panel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.parent_panel.ColumnCount = 1;
            this.parent_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.parent_panel.Controls.Add(this.show_child_btn, 0, 1);
            this.parent_panel.Controls.Add(this.parent_grid, 0, 0);
            this.parent_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parent_panel.Location = new System.Drawing.Point(0, 0);
            this.parent_panel.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.parent_panel.Name = "parent_panel";
            this.parent_panel.RowCount = 2;
            this.parent_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90.06622F));
            this.parent_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.933775F));
            this.parent_panel.Size = new System.Drawing.Size(441, 302);
            this.parent_panel.TabIndex = 0;
            // 
            // show_child_btn
            // 
            this.show_child_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.show_child_btn.Location = new System.Drawing.Point(4, 273);
            this.show_child_btn.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.show_child_btn.Name = "show_child_btn";
            this.show_child_btn.Size = new System.Drawing.Size(433, 27);
            this.show_child_btn.TabIndex = 0;
            this.show_child_btn.Text = "Show children";
            this.show_child_btn.UseVisualStyleBackColor = true;
            this.show_child_btn.Click += new System.EventHandler(this.OnShowChildrenClicked);
            // 
            // parent_grid
            // 
            this.parent_grid.AllowUserToAddRows = false;
            this.parent_grid.AllowUserToDeleteRows = false;
            this.parent_grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.parent_grid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.parent_grid.BackgroundColor = System.Drawing.Color.Brown;
            this.parent_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.parent_grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.parent_grid.GridColor = System.Drawing.Color.YellowGreen;
            this.parent_grid.Location = new System.Drawing.Point(4, 2);
            this.parent_grid.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.parent_grid.Name = "parent_grid";
            this.parent_grid.ReadOnly = true;
            this.parent_grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.parent_grid.Size = new System.Drawing.Size(433, 267);
            this.parent_grid.TabIndex = 1;
            this.parent_grid.SelectionChanged += new System.EventHandler(this.OnSelectionChanged);
            this.parent_grid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyDown);
            // 
            // ParentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(441, 302);
            this.Controls.Add(this.parent_panel);
            this.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "ParentForm";
            this.Text = "Parent window";
            this.parent_panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.parent_grid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel parent_panel;
        private System.Windows.Forms.Button show_child_btn;
        private System.Windows.Forms.DataGridView parent_grid;
    }
}

