﻿namespace lab_4
{
    partial class ChildForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.child_panel = new System.Windows.Forms.TableLayoutPanel();
            this.child_grid = new System.Windows.Forms.DataGridView();
            this.add_child_btn = new System.Windows.Forms.Button();
            this.child_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.child_grid)).BeginInit();
            this.SuspendLayout();
            // 
            // child_panel
            // 
            this.child_panel.ColumnCount = 1;
            this.child_panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.child_panel.Controls.Add(this.child_grid, 0, 0);
            this.child_panel.Controls.Add(this.add_child_btn, 0, 1);
            this.child_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.child_panel.Location = new System.Drawing.Point(0, 0);
            this.child_panel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.child_panel.Name = "child_panel";
            this.child_panel.RowCount = 2;
            this.child_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 92.1875F));
            this.child_panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.8125F));
            this.child_panel.Size = new System.Drawing.Size(446, 383);
            this.child_panel.TabIndex = 0;
            // 
            // child_grid
            // 
            this.child_grid.AllowUserToAddRows = false;
            this.child_grid.AllowUserToOrderColumns = true;
            this.child_grid.BackgroundColor = System.Drawing.Color.Brown;
            this.child_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.child_grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.child_grid.GridColor = System.Drawing.Color.YellowGreen;
            this.child_grid.Location = new System.Drawing.Point(4, 3);
            this.child_grid.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.child_grid.Name = "child_grid";
            this.child_grid.Size = new System.Drawing.Size(438, 347);
            this.child_grid.TabIndex = 1;
            this.child_grid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.OnCellValueChanged);
            this.child_grid.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.OnRowRemoved);
            // 
            // add_child_btn
            // 
            this.add_child_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.add_child_btn.Location = new System.Drawing.Point(4, 356);
            this.add_child_btn.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.add_child_btn.Name = "add_child_btn";
            this.add_child_btn.Size = new System.Drawing.Size(438, 24);
            this.add_child_btn.TabIndex = 2;
            this.add_child_btn.Text = "Add child";
            this.add_child_btn.UseVisualStyleBackColor = true;
            this.add_child_btn.Click += new System.EventHandler(this.OnAddChildClicked);
            // 
            // ChildForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(446, 383);
            this.Controls.Add(this.child_panel);
            this.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "ChildForm";
            this.Text = "Child window";
            this.child_panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.child_grid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel child_panel;
        private System.Windows.Forms.DataGridView child_grid;
        private System.Windows.Forms.Button add_child_btn;
    }
}