﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace lab_4
{
    public partial class InputForm : Form
    {
        public InputForm()
        {
            InitializeComponent();
            input_results = new List<string>();
            is_ok_pressed = false;
        }

        public List<string> input
        {
            get
            {
                if (is_ok_pressed)
                    return input_results;
                return null;
            }
        }

        public bool is_ok
        {
            get
            {
                return is_ok_pressed;
            }
        }

        public List<string> labels
        {
            set
            {
                foreach (string label in value)
                {
                    Label control = new Label();
                    control.Text = label;
                    data_panel.Controls.Add(control);
                    data_panel.Controls.Add(new TextBox());
                }
            }
        }

        private void OnOKClicked(object sender, EventArgs e)
        {
            is_ok_pressed = true;

            foreach(Control ctrl in data_panel.Controls)
            {     
                if (ctrl.GetType() == typeof(TextBox))
                {
                    TextBox input = ctrl as TextBox;
                    input_results.Add(input.Text);
                }
            }

            Close();
        }

        private void OnCancelClicked(object sender, EventArgs e)
        {
            Close();
        }

        private bool is_ok_pressed;
        private List<string> input_results;
    }
}
