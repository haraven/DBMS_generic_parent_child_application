﻿using lab_4.Utils;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace lab_4
{
    public partial class ChildForm : Form
    {
        public ChildForm(SqlConnection conn, string table, string table_fk)
        {
            InitializeComponent();

            connection    = conn;
            data_set      = new DataSet();
            data_adapter  = new SqlDataAdapter();

            child_table   = table;
            child_fk_colname      = table_fk;
            child_pk_colname      = TableHelper.GetTablePK(connection, child_table);
            child_pk_type = TableHelper.GetColType(connection, child_table, child_pk_colname);
            child_fk_type = TableHelper.GetColType(connection, table, child_fk_colname);
            parent        = "";
        }

        private void FillGrid()
        {
            string select_query = @"SELECT * FROM " + child_table +
                                " WHERE " + child_fk_colname + " = @Param";
            SqlCommand select = new SqlCommand(select_query, connection);
            select.Parameters.Add("@Param", child_fk_type).Value = parent;
            data_adapter.SelectCommand = select;
            data_set.Clear();
            data_adapter.Fill(data_set);

            child_grid.DataSource = data_set.Tables[0];
            foreach (DataGridViewColumn col in child_grid.Columns)
            {
                if (col.Name.Equals(child_fk_colname))
                    col.Visible = false;
            }
        }

        public string parent_id
        {
            get
            {
                return parent;
            }
            set
            {
                parent = value;
                FillGrid();
            }
        }

        private void OnCellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            DataGridViewRow current_row = child_grid.CurrentRow;
            List<string> column_list    = new List<string>();
            List<string> value_list     = new List<string>();

            foreach (DataGridViewCell cell in current_row.Cells)
            {
                if (cell.Visible)
                {
                    column_list.Add(cell.OwningColumn.Name);
                    value_list.Add(cell.Value.ToString());
                }
            }
            string update_query = "UPDATE " + child_table + " SET ";
            for (int i = 0; i < column_list.Count; ++i)
            {
                update_query += column_list[i] + " = @" + column_list[i];
                if (i < column_list.Count - 1)
                    update_query += ", ";
            }
            string pk = TableHelper.GetTablePK(connection, child_table);
            update_query += " WHERE " + pk + " = " + current_row.Cells[pk].Value.ToString();

            SqlCommand update = new SqlCommand(update_query, connection);
            for (int i = 0; i < value_list.Count; ++i)
            {
                SqlDbType coltype = TableHelper.GetColType(connection, child_table, column_list[i]);
                if (value_list[i].Equals(""))
                    update.Parameters.Add("@" + column_list[i], coltype).Value = System.DBNull.Value;
                else
                    update.Parameters.Add("@" + column_list[i], coltype).Value = value_list[i];
            }

            update.ExecuteNonQuery();
        }

        private void OnRowRemoved(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (e.Cancel)
                return;

            DataGridViewRow deleted_row = e.Row;

            string delete_query = @"DELETE FROM " + child_table + 
                " WHERE " + child_pk_colname + " = @" + child_pk_colname;
            SqlCommand delete = new SqlCommand(delete_query, connection);
            delete.Parameters.Add("@" + child_pk_colname, child_pk_type).Value = deleted_row.Cells[child_pk_colname].Value;
            delete.ExecuteNonQuery();
        }

        private List<string> GetColNames()
        {
            List<string> column_list = new List<string>();
            foreach (DataGridViewColumn column in child_grid.Columns)
                if (column.Name != child_fk_colname)
                column_list.Add(column.Name + ':');

            return column_list;
        }

        private void OnAddChildClicked(object sender, System.EventArgs e)
        {
            InputForm input = new InputForm();
            List<string> col_list = GetColNames();
            input.labels = col_list;
            input.ShowDialog();
            string insert_query = "INSERT INTO " + child_table + " (";
            for (int i = 0; i < col_list.Count; ++i)
            {
                insert_query += col_list[i].Substring(0, col_list[i].Length - 1);
                insert_query += ", ";
            }

            insert_query += child_fk_colname + ") VALUES (";

            if (input.is_ok)
            {
                List<string> res = input.input;
                for (int i = 0; i < res.Count; ++i)
                {
                    SqlDbType coltype = TableHelper.GetColType(connection, child_table, col_list[i].Substring(0, col_list[i].Length - 1));
                    if (res[i] == "")
                        insert_query += "NULL";
                    else if (coltype == SqlDbType.VarChar || coltype == SqlDbType.NVarChar)
                    {
                        insert_query += "'" + res[i] + "'";
                    }
                    else
                        insert_query += res[i];

                    insert_query += ", ";
                }

                insert_query += parent + ')';
                MessageBox.Show(insert_query);
                SqlCommand insert = new SqlCommand(insert_query, connection);
                insert.ExecuteNonQuery();
                FillGrid();
            }
        }

        private string child_table;
        private string parent;
        private string child_fk_colname;
        private string child_pk_colname;
        private SqlDbType child_pk_type;
        private SqlDbType child_fk_type;
        private DataSet data_set;
        private SqlConnection connection;
        private SqlDataAdapter data_adapter;
    }
}
