﻿using lab_4.Utils;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace lab_4
{
    public partial class ParentForm : Form
    {
        public ParentForm(string connection_string, string parent_table_name, string child_table_name, string child_foreign_key)
        {
            InitializeComponent();

            parent_table           = parent_table_name;
            child_table            = child_table_name;

            connection             = new SqlConnection(connection_string);
            data_set               = new DataSet();
            string select_query    = "SELECT * FROM " + parent_table;
            SqlCommand select      = new SqlCommand(select_query, connection);
            data_adapter           = new SqlDataAdapter(select);
            data_adapter.Fill(data_set);
            parent_grid.DataSource = data_set.Tables[0];

            table_pk               = TableHelper.GetTablePK(connection, parent_table);
            child_fk               = child_foreign_key;

            child_form             = new ChildForm(connection, child_table, child_fk);
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                connection.Close();
                Close();
            }
        }

        private void UpdateChildData()
        {
            DataGridViewRow current_row = parent_grid.CurrentRow;
            string val = current_row.Cells[table_pk].Value.ToString();
            child_form.parent_id = val;
        }

        private void OnShowChildrenClicked(object sender, System.EventArgs e)
        {
            if (child_form.IsDisposed)
                child_form = new ChildForm(connection, child_table, child_fk);

            UpdateChildData();
            child_form.Show();
        }

        private void OnSelectionChanged(object sender, System.EventArgs e)
        {
            UpdateChildData();
        }

        private string parent_table;
        private string child_table;
        private string table_pk;
        private string child_fk;
        private DataSet data_set;
        private SqlConnection connection;
        private SqlDataAdapter data_adapter;
        private ChildForm child_form;
    }
}
